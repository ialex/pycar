from django.db import models
from apps.autos.models import Autos
from apps.clientes.models import Clientes
from django.contrib.auth.models import User
from datetime import datetime


class Ventas(models.Model): 
    """ 
    Modelo de Rentas de los autos
    Require modelos:
        Autos
        Clientes"""
    TIPOS_PAGO = (
        ('EFECTIVO','EFECTIVO'),
        ('TARJETA', 'TARJETA'),
        ('PENDIENTE', 'PENDIENTE'),
    )
    TIPOS_TARJETA = (
        ('VISA', 'VISA'),
        ('AMERICAN', 'AMERICAN EXPRESS'),
        ('MASTERCARD', 'MASTERCARD'),
    )
    SERIE = (
        ('C','C'), # Fiscal
        ('D', 'D'), # No fiscal
    )
    auto = models.ForeignKey(Autos)
    cliente = models.ForeignKey(Clientes)
    vendedor = models.ForeignKey(User)
    km_entrada = models.PositiveIntegerField(default=0)
    km_salida = models.PositiveIntegerField(default=0)
    km_recorridos = models.PositiveIntegerField(default=0)
    finalizada = models.BooleanField(default=False)
    fecha_salida = models.DateTimeField()
    created_at = models.DateTimeField(default=datetime.now())
    hora_extra = models.IntegerField(default = 0)
    fecha_entrada = models.DateTimeField(null=True)
    preciodia = models.FloatField(null=True, verbose_name="Precio Por Dia")
    seguro_accidentes = models.BooleanField(default=False)
    seguro_colision = models.BooleanField(default=False)
    tiene_conductor = models.BooleanField(default=False)
    nombre_conductor = models.CharField(max_length=500, blank=True, null=True)
    licencia_conductor = models.CharField(blank=True, null=True, max_length=100)
    lic_vencimiento_conductor = models.DateField(blank=True, null=True)
    lic_expedicion_conductor = models.DateField(blank=True, null=True)
    deposito = models.FloatField(default=0)
    adelanto = models.FloatField(default=0)
    iva = models.BooleanField(verbose_name="Con/Sin IVA")
    gas_octavos_entrada = models.IntegerField(null=True, 
                            verbose_name="Octavos de Gas")
    gas_octavos_salida = models.IntegerField(verbose_name="Octavos de Gas")
    reembolso = models.FloatField(default=0)
    total_iva = models.FloatField(default=0)
    total_horaextra = models.FloatField(default=0)
    total_dias = models.FloatField(default=0) # Dinero de renta
    total_gas = models.FloatField(default=0)
    total_seguros = models.FloatField(default=0)
    total_conductor = models.FloatField(default=0)
    total = models.FloatField(null=True)
    danios = models.FloatField(default=0)
    dias = models.PositiveIntegerField() # Numero dias que se rento
    tipo_pago = models.CharField(max_length=30, choices=TIPOS_PAGO)
    tarjeta = models.CharField(max_length=50, choices=TIPOS_TARJETA,
        verbose_name="Tipo de Tarjeta", null=True, blank=True)
    tar_vencimiento = models.DateField(verbose_name="Fecha Vencimiento", 
                                        null=True, blank=True)
    tar_numero = models.IntegerField(verbose_name="Numero Tarjeta", 
                                     blank=True ,null=True)
    num_contrato = models.PositiveIntegerField()
    serie = models.CharField(max_length=1, choices=SERIE)
    comentarios = models.TextField(max_length=255, blank=True)


class Reservaciones(models.Model):
    """
    Modelo de reservaciones
     Require modelos:
        Autos
        Clientes"""
    auto = models.ForeignKey(Autos)
    cliente = models.CharField(max_length=500, blank=True)
    fecha = models.DateTimeField()
    hotel = models.CharField(max_length=500, blank=True, 
            verbose_name="Hotel")
    num_cuarto = models.CharField(max_length=500, blank=True, 
            verbose_name="Num. Cuarto")
    anticipo = models.CharField(max_length=500, blank=True)
    comentarios = models.TextField(max_length=255, blank=True)
    created_at = models.DateTimeField(default=datetime.now())
