from django.contrib import admin
from apps.ventas.models import Ventas, Reservaciones


class VentasAdmin(admin.ModelAdmin):
    pass


class ReservacionesAdmin(admin.ModelAdmin):
    pass


admin.site.register(Reservaciones, ReservacionesAdmin)
admin.site.register(Ventas, VentasAdmin)

