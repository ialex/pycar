# python imports
import os
import settings

# django imports
from django.http import HttpResponse
from django.shortcuts import get_object_or_404


# reportlab imports
from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_JUSTIFY
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.platypus import Table
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.lib import colors
from reportlab.pdfgen import canvas


# local imports
from apps.ventas.models import Reservaciones


def print_reserva(request, id):
    reservacion = get_object_or_404(Reservaciones, pk=id)

    # Creating the pdf response
    response = HttpResponse(mimetype='application/pdf')
    response['Content-Disposition'] = 'attachment; filename=Reservacion.pdf'

    width, heigth = letter
    heigth = int(heigth/2)
    print str(heigth) + " " + str(width)
    pdf = canvas.Canvas(response, pagesize=(width, heigth))
    pdf.setLineWidth(.3)
    pdf.setFont('Helvetica', 12)
     
    #Paper size width=612 heigth=396
    #Header Logo
    path = os.path.join(settings.SITE_ROOT, 'static/assets/img/') + "header.jpg"
    pdf.drawImage(path, 160, 290, width=250, height=96)

    #Layout
    #Main Rect
    pdf.roundRect(5, 5, width-20, heigth-20, 30, stroke=1, fill=0)
    
    #Fields
    # Reservation number field
    pdf.roundRect(20, 250, width-350, heigth-370, 5, stroke=1, fill=0)
    pdf.drawString(25, 255, "Reservacion No:         " + str(reservacion.id))
    # Client field
    pdf.roundRect(20, 220, width-50, heigth-370, 5, stroke=1, fill=0)
    pdf.drawString(25, 225, "Cliente:     " + str(reservacion.cliente).title())
    # Car field
    car_desc = "%s %s %s" % (reservacion.auto.modelo, str(reservacion.auto.anio), reservacion.auto.color)
    pdf.roundRect(20, 190, width-350, heigth-370, 5, stroke=1, fill=0)
    pdf.drawString(25, 195, "Auto:        " + car_desc.title())
    # Date field
    pdf.roundRect(width-330, 190, width-310, heigth-370, 5, stroke=1, fill=0)
    pdf.drawString(width-310, 195, "Fecha:     " + reservacion.fecha.strftime("%d/%m/%Y %H:%M"))

    # Hotel field
    pdf.roundRect(20, 160, 200, heigth-370, 5, stroke=1, fill=0)
    pdf.drawString(25, 165, "Hotel:      " + reservacion.hotel.title())
    # Room Number field
    pdf.roundRect(220, 160, 160, heigth-370, 5, stroke=1, fill=0)
    pdf.drawString(225, 165, "No. Cuarto:     " + reservacion.num_cuarto)
    # Anticipo field
    pdf.roundRect(380, 160, 205, heigth-370, 5, stroke=1, fill=0)
    pdf.drawString(385, 165, "Anticipo:     $ " + reservacion.anticipo)
    # Comments field
    pdf.drawString(25, 145, "Comentarios:")
    pdf.roundRect(20, 15, 565, 130, 5, stroke=1, fill=0)
    # Actual comment
    text = pdf.beginText(25, 130)
    text.textLines(reservacion.comentarios)
    pdf.drawText(text)

    # Closing document
    pdf.showPage()
    pdf.save()
    # Returns pdf as response
    return response

    



