# encoding: latin-1
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.contrib.auth.decorators import permission_required, login_required
from django.template import RequestContext
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from apps.ventas.models import Ventas, Reservaciones
from apps.ventas.forms import ReservacionesForm, VentasCompleteForm, \
     VentasForm, VentasReturnForm, ReporteVentasForm
from django.contrib import messages
from apps.admin.models import Precios
from apps.eonutils.forms import DivErrorList
from datetime import datetime, time
from django.db.models import Sum, Q


@login_required
def reservaciones(request):
    reservacion = Reservaciones.objects.all().order_by('fecha')
    paginator = Paginator(reservacion, 30) 
    
     # Make sure page request is an int. If not, deliver first page.
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        reservacion_pagina = paginator.page(page)
    except (EmptyPage, InvalidPage):
        reservacion_pagina = paginator.page(paginator.num_pages)
    
    vars = { 'reservaciones' : reservacion_pagina }
    
    return render_to_response('ventas/reservaciones.html', vars,
                                 RequestContext(request))


@permission_required('ventas.delete_reservaciones')
def del_reservaciones(request, id):
    reservacion = get_object_or_404(Reservaciones, pk = id)
    reservacion.delete()
    return redirect('/ventas/reservaciones/?s=1')


@permission_required('ventas.add_reservaciones')
def add_reservaciones(request):
    if request.method == 'POST':
        form = ReservacionesForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/ventas/reservaciones/', 
                            context_instance = RequestContext(request))
    else:
        form = ReservacionesForm()
    vars = {'form' : form}
    return render_to_response('ventas/add_reservaciones.html', vars,
                                    context_instance = RequestContext(request))


@permission_required('ventas.change_reservaciones')
def edit_reservaciones(request, id):
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    
    if request.method == 'POST':
        reservacion = get_object_or_404(Reservaciones, pk = id)
        form = ReservacionesForm(request.POST, instance=reservacion)
        if form.is_valid():
            form.save()
        return redirect('/ventas/reservaciones/?page=' + str(page), 
                            context_instance = RequestContext(request))
    else:
        reservacion = get_object_or_404(Reservaciones, pk = id)
        form = ReservacionesForm(instance = reservacion)
    vars = { 'form' : form, 'reservacion' : reservacion, 'page' : page }
    return render_to_response('ventas/change_reservaciones.html', vars,
                                context_instance = RequestContext(request))


@permission_required('ventas.add_ventas')
def vender_reservaciones(request, id):
    reserva = get_object_or_404(Reservaciones, pk=id)
    form = VentasForm(
                        {'fecha_salida': reserva.fecha,
                         'auto': reserva.auto,
                         'comentarios': reserva.comentarios,
                         'adelanto': reserva.anticipo,
                        }
                    )
    reserva.delete()
    messages.success(request, 'Se ha eliminado de la lista de reservaciones.')
    vars = {'form': form}
    return render_to_response('ventas/add_ventas.html', vars,
                                    context_instance = RequestContext(request))


# Ventas
@login_required
def ventas(request):
    ventas = Ventas.objects.all().order_by('serie').order_by('-num_contrato')
    paginator = Paginator(ventas, 20) 
    
     # Make sure page request is an int. If not, deliver first page.
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        venta_pagina = paginator.page(page)
    except (EmptyPage, InvalidPage):
        venta_pagina = paginator.page(paginator.num_pages)
    
    vars = { 'ventas' : venta_pagina }
    
    return render_to_response('ventas/ventas.html', vars,
                                 RequestContext(request))


@permission_required('ventas.delete_ventas')
def del_ventas(request, id):
    venta = get_object_or_404(Ventas, pk = id)
    venta.delete()
    messages.error(request, 'La venta ha sido eliminada.')
    return redirect('/ventas/list/')


@permission_required('ventas.add_ventas')
def add_ventas(request):
    if request.method == 'POST':
        form = VentasForm(request.POST, error_class=DivErrorList)
        if form.is_valid():
            venta = form.save(commit=False)
            venta.vendedor = request.user
            if venta.tipo_pago == "PENDIENTE":
                venta.reembolso = 1
            venta.save()
            messages.success(request, 'Venta agregada correctamente.')
            return redirect('/ventas/entregas/', 
                            context_instance = RequestContext(request))
    else:
        form = VentasForm()
    vars = {'form' : form}
    return render_to_response('ventas/add_ventas.html', vars,
                                    context_instance = RequestContext(request))


@permission_required('ventas.change_ventas')
def edit_ventas(request, id):
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    
    if request.method == 'POST':
        venta = get_object_or_404(Ventas, pk=id)
        form = VentasForm(request.POST, instance=venta, 
                error_class=DivErrorList)
        if form.is_valid():
            venta = form.save(commit=False)
            venta.vendedor = request.user
            venta.save()
            messages.success(request, 'Venta Actualizada.')
            return redirect('/ventas/list/?page=' + str(page), 
                            context_instance=RequestContext(request))
    else:
        venta = get_object_or_404(Ventas, pk=id)
        form = VentasForm(instance=venta)
    vars = {'form': form, 'venta': venta, 'page': page}
    return render_to_response('ventas/change_ventas.html', vars,
                                context_instance=RequestContext(request))


@login_required
def detail_ventas(request, id):
    venta = get_object_or_404(Ventas, pk=id)
    vars = {'venta': venta}
    return render_to_response('ventas/checkout.html', vars,
                                context_instance=RequestContext(request))


@login_required
def lista_entregas(request):
    entregas = Ventas.objects.filter(finalizada = False)
    entregas = entregas.order_by('-num_contrato')
    
    vars = {'ventas': entregas}
    return render_to_response('ventas/lista_entregas.html', vars,
                                context_instance=RequestContext(request))


@login_required
def entregar(request, id):
    if request.method == 'POST':
        venta = get_object_or_404(Ventas, pk=id)
        form = VentasReturnForm(request.POST, instance=venta)
        if form.is_valid():
            precios = Precios.objects.latest('hora_extra')
            f = form.save(commit=False)
            f.finalizada = True
            days = abs((f.fecha_salida.date() - f.fecha_entrada.date()).days)
            f.total_dias = f.preciodia * days
            f.total_horaextra = f.hora_extra * precios.hora_extra
            f.total_gas = (f.gas_octavos_salida - f.gas_octavos_entrada) * precios.octavo_gas 
            f.total = f.total_gas + f.total_dias + f.total_horaextra
            f.km_recorridos = f.km_entrada - f.km_salida
            if f.seguro_accidentes:
                f.total += precios.seguro_accidentes
                f.total_seguros = precios.seguro_accidentes 
            if f.seguro_colision:
                f.total += precios.seguro_colision * days
                f.total_seguros += precios.seguro_colision * days
            if f.iva:
                f.total_iva = f.total * 0.11
                f.total += f.total * 0.11
            if f.tiene_conductor:
                f.total_conductor = precios.conductor
                f.total += precios.conductor
            f.save()
            messages.success(request, 'Entrega Registrada.')
            return redirect('/ventas/checkout/%s' % venta.id, 
                            context_instance=RequestContext(request))
    else:
        venta = get_object_or_404(Ventas, pk=id)
        form = VentasReturnForm(instance=venta)
    vars = {'form': form, 'venta': venta}
    return render_to_response('ventas/devolver.html', vars,
                                context_instance=RequestContext(request))


@login_required
def checkout(request, id):
    venta = get_object_or_404(Ventas, pk=id)
    if venta.finalizada:
        vars = {'venta' : venta}
        return render_to_response('ventas/checkout.html', vars,
                                context_instance=RequestContext(request))
    else:
        return redirect('/ventas/devolver/%s' % venta.id)


def terminadas(request):
    if request.method == 'POST':
        q = request.POST["q"]
        finalizadas = Ventas.objects.filter(
                Q(num_contrato__icontains=q)
        )
    else:
        finalizadas = Ventas.objects.filter(finalizada = True)
    finalizadas = finalizadas.order_by('serie')
    finalizadas = finalizadas.order_by('-num_contrato')
    
    paginator = Paginator(finalizadas, 20) 
    
     # Make sure page request is an int. If not, deliver first page.
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        ventas = paginator.page(page)
    except (EmptyPage, InvalidPage):
        ventas = paginator.page(paginator.num_pages)
    vars = {'ventas' : ventas}
    return render_to_response('ventas/terminadas.html', vars,
                                context_instance=RequestContext(request))


def reportes(request):
    if request.method == 'POST':
        form = ReporteVentasForm(request.POST)
        if form.is_valid():
            if request.POST['fecha_inicial'] and request.POST['fecha_final']:
            
                fecha_inicio_min = datetime.combine(form.cleaned_data['fecha_inicial'], time.min)
                fecha_final_max = datetime.combine(form.cleaned_data['fecha_final'], time.max)
                ventas = Ventas.objects.filter(fecha_entrada__range= 
                    (fecha_inicio_min, fecha_final_max), finalizada=True)
            else:
                mes = datetime.today().month
                ventas = Ventas.objects.filter(fecha_entrada__month=mes, finalizada=True)
            if form.cleaned_data['tipo_contrato']:
                ventas = ventas.filter(serie=form.cleaned_data['tipo_contrato'])
            if form.cleaned_data['auto']:
                ventas = ventas.filter(auto=form.cleaned_data['auto'])
            if form.cleaned_data['vendedor']:
                ventas = ventas.filter(vendedor=form.cleaned_data['vendedor'])
            total = ventas.aggregate(Sum('total'))
            total_fiscal = ventas.filter(serie="C").aggregate(Sum('total'))
            total_no_fiscal = ventas.filter(serie="D").aggregate(Sum('total'))
    else:
        form = ReporteVentasForm()
        ventas = {}
        total_fiscal = 0
        total = 0
        total_no_fiscal = 0
    vars = {'form': form, 'ventas': ventas, 'total': total, 
        'total_fiscal': total_fiscal, 'total_no_fiscal': total_no_fiscal}
    return render_to_response('ventas/reporte_ventas.html', vars,
                                context_instance=RequestContext(request))


def pendientes(request):
    pendientes = Ventas.objects.filter(tipo_pago = "PENDIENTE", reembolso=1)
    pendientes = pendientes.order_by('serie')
    pendientes = pendientes.order_by('-num_contrato')
    
    paginator = Paginator(pendientes, 20) 
    
     # Make sure page request is an int. If not, deliver first page.
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        ventas = paginator.page(page)
    except (EmptyPage, InvalidPage):
        ventas = paginator.page(paginator.num_pages)
    vars = {'ventas' : ventas}
    return render_to_response('ventas/pendientes.html', vars,
                                context_instance=RequestContext(request))
