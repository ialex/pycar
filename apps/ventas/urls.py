from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('apps.ventas.views',
    # URLs Ventas
    url(r'list/', 'ventas'),
    url(r'del/(?P<id>\d+)', 'del_ventas'),
    url(r'edit/(?P<id>\d+)', 'edit_ventas'),
    url(r'add/', 'add_ventas'),
    url(r'detail/(?P<id>\d+)', 'detail_ventas'),
    url(r'entregas/$', 'lista_entregas'), # ACTIVOS
    url(r'devolver/(?P<id>\d+)/$', 'entregar'),
    url(r'checkout/(?P<id>\d+)/$', 'checkout'),
    url(r'finished/$', 'terminadas'), # TERMINADAS
    url(r'reportes/$', 'reportes'),
    url(r'pendientes/$', 'pendientes'), # CREDITO
    # URL Reservaciones
    url(r'reservaciones', 'reservaciones'),
    url(r'reservacion/borrar/(?P<id>\d+)', 'del_reservaciones'),
    url(r'reservacion/editar/(?P<id>\d+)', 'edit_reservaciones'),
    url(r'reservacion/agregar/', 'add_reservaciones'),
    url(r'reservacion/vender/(?P<id>\d+)/$', 'vender_reservaciones'),
)

urlpatterns += patterns('apps.ventas.reports.pdf',
    url(r'reservacion/print/(?P<id>\d+)/$', 'print_reserva', name='print-reserva'),
)
