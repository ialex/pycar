from django import forms
from apps.ventas.models import Ventas, Reservaciones
from apps.autos.models import Autos
from django.contrib.auth.models import User

CHOICES = (
            ('1', '1/8'),
            ('2', '2/8'),
            ('3', '3/8'),
            ('4', '4/8'),
            ('5', '5/8'),
            ('6', '6/8'),
            ('7', '7/8'),
            ('8', '8/8'),
        )


class VentasForm(forms.ModelForm):
    class Meta:
        
        model = Ventas
        exclude = (
            'created_at', 'finalizada', 'hora_extra', 'total_seguros',
            'gas_octavos_entrada', 'reembolso', 'danios', 'total_conductor',
            'total_dias', 'total_gas', 'total_horaextra', 'total_iva',
            'km_entrada', 'km_recorridos', 'vendedor',
            
        )
        widgets = {
            'lic_vencimiento_conductor': forms.TextInput(attrs={'class': 'dp'}),
            'lic_expedicion_conductor': forms.TextInput(attrs={'class': 'dp'}),
            'fecha_entrada' : forms.DateTimeInput(attrs={'class': 'dtp'}),
            'fecha_salida' : forms.DateTimeInput(attrs={'class': 'dtp'}),
            'gas_octavos_salida' : forms.RadioSelect(choices=CHOICES),
            'auto': forms.Select(attrs={'class': 'span6 completeselect'}),
            'cliente': forms.Select(attrs={'class': 'span6 completeselect'}),
            'chofer': forms.Select(attrs={'class': 'span6 completeselect'}),
            'comentarios': forms.Textarea(attrs={'class': 'span6'}),
            'dias' : forms.HiddenInput(),
            'serie': forms.Select(attrs={'class': 'span1'}),
            'total': forms.HiddenInput(),
            'tar_vencimiento': forms.DateInput(attrs={'class': 'dp'}),
        }


class VentasCompleteForm(forms.ModelForm):
    class Meta:
        model = Ventas


class ReservacionesForm(forms.ModelForm):
    class Meta:
        model = Reservaciones
        exclude = ('created_at')
        widgets = {
            'fecha' : forms.DateTimeInput(attrs={'class': 'dtp'}),
            'cliente':  forms.TextInput(attrs={'class': 'span6'}),
            'auto':  forms.Select(attrs={'class': 'span6 completeselect'}),
            'comentarios': forms.Textarea(attrs={'class': 'span6'})
        }


class VentasReturnForm(forms.ModelForm):
    class Meta:
        model = Ventas
        exclude = (
            'created_at', 'finalizada', 'total', 'reembolso', 'preciodia',
            'gas_octavos_salida', 'auto', 'cliente', 'chofer', 'fecha_salida',
            'seguro_accidentes', 'seguro_colision', 'adelanto', 'iva',
            'total_dias', 'total_gas', 'total_horaextra', 'total_iva',
            'total_seguros', 'dias', 'num_contrato', 'serie',
            'deposito', 'total_conductor', 'km_recorridos', 'km_salida',
            'vendedor', 'tiene_conductor', 'nombre_conductor', 'tar_numero',
            'lic_vencimiento_conductor','licencia_conductor', 'tar_vencimiento',
            'lic_expedicion_conductor', 'tarjeta',
        )
        widgets = {
            'fecha_entrada' : forms.DateTimeInput(attrs={'class': 'dtp'}),
            'comentarios': forms.Textarea(attrs={'class': 'span6'}),
            'gas_octavos_entrada' : forms.RadioSelect(choices=CHOICES),
        }


class ReporteVentasForm(forms.Form):
    SERIE = (
        ('', '------'),
        ('C','C'), # Fiscal
        ('D', 'D'), # No fiscal
    )
    fecha_inicial = forms.DateField(required=False, 
        widget=forms.DateInput(attrs={'class': 'dp'}))
    fecha_final = forms.DateField(required=False,
        widget=forms.DateInput(attrs={'class': 'dp'}))
    vendedor = forms.ModelChoiceField(required=False, 
        queryset=User.objects.filter(is_staff=False))
    auto = forms.ModelChoiceField(required=False, queryset=Autos.objects.all(),
        widget=forms.Select(attrs={'class': 'completeselect'})
    )
    tipo_contrato = forms.ChoiceField(choices=SERIE, required=False)

