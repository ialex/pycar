from django import forms
from apps.clientes.models import Clientes


class ClientesForm(forms.ModelForm):
    class Meta:
        model = Clientes
        widgets = {
            'lic_vencimiento': forms.DateInput(attrs={'class': 'dp'}),
            'lic_expedicion': forms.DateInput(attrs={'class': 'dp'}),
            'edad': forms.TextInput(attrs={'class': 'span2'}),
            'nombre': forms.TextInput(attrs={'class': 'span6'}),
            'apellidos': forms.TextInput(attrs={'class': 'span6'}),
            'domicilio': forms.TextInput(attrs={'class': 'span8'}),
            'email': forms.TextInput(attrs={'class': 'span6'}),
            'fis_domicilio': forms.TextInput(attrs={'class': 'span8 disabled'}),
            'fis_nombre': forms.TextInput(attrs={'class': 'span6 disabled'}),
            'RFC': forms.TextInput(attrs={'class': 'disabled'}),
            'fis_telefono': forms.TextInput(attrs={'class': 'disabled'}),
        }
