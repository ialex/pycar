from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('apps.clientes.views',
    # URLs Clientes
    url(r'list/', 'clientes'),
    url(r'del/(?P<id>\d+)', 'del_clientes'),
    url(r'edit/(?P<id>\d+)/', 'edit_clientes'),
    url(r'add/', 'add_clientes'),
    url(r'detail/(?P<id>\d+)', 'detail_clientes'),
    url(r'api/get/(?P<id>\d+)', 'api_get_client'),
)
