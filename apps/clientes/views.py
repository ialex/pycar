from django.shortcuts import render_to_response, get_object_or_404, redirect, \
    get_list_or_404
from django.contrib.auth.decorators import permission_required, login_required
from django.template import RequestContext
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from apps.clientes.models import Clientes
from apps.clientes.forms import ClientesForm
from django.contrib import messages
from apps.eonutils.forms import DivErrorList
from django.core import serializers
from django.utils import simplejson
from django.http import HttpResponse
from django.db.models import Q


@login_required
def clientes(request):
    if request.method == 'POST':
        q = request.POST["q"]
        clientes = Clientes.objects.filter(
                            Q(nombre__icontains=q)|
                            Q(apellidos__icontains=q)
                            )
    else:
        clientes = Clientes.objects.all().order_by('nombre')
    paginator = Paginator(clientes, 30) 
    
     # Make sure page request is an int. If not, deliver first page.
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        clientes_pagina = paginator.page(page)
    except (EmptyPage, InvalidPage):
        clientes_pagina = paginator.page(paginator.num_pages)
    
    vars = { 'clientes' : clientes_pagina }
    
    return render_to_response('clientes/clientes.html', vars,
                                 RequestContext(request))


@permission_required('clientes.delete_clientes')
def del_clientes(request, id):
    cliente = get_object_or_404(Clientes, pk = id)
    cliente.delete()
    messages.error(request, 'El Cliente ha sido eliminado')
    return redirect('/clientes/list')


@permission_required('clientes.add_clientes')
def add_clientes(request):
    if request.method == 'POST':
        form = ClientesForm(request.POST, error_class=DivErrorList)
        if form.is_valid():
            form.save()
            messages.success(request, 'El cliente ha sido agregado.')
            return redirect('/clientes/list/', 
                            context_instance = RequestContext(request))
    else:
        form = ClientesForm(error_class=DivErrorList)
    vars = {'form' : form}
    return render_to_response('clientes/add_clientes.html', vars,
                                    context_instance = RequestContext(request))


@permission_required('clientes.change_clientes')
def edit_clientes(request, id):
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    
    if request.method == 'POST':
        cliente = get_object_or_404(Clientes, pk = id)
        form = ClientesForm(request.POST, instance=cliente, 
                                    error_class=DivErrorList)
        if form.is_valid():
            form.save()
            messages.success(request, 'El cliente ha sido actualizado.')
            return redirect('/clientes/list/?page=' + str(page), 
                            context_instance = RequestContext(request))
    else:
        cliente = get_object_or_404(Clientes, pk = id)
        form = ClientesForm(instance=cliente, error_class=DivErrorList)
    vars = { 'form' : form, 'cliente' : cliente, 'page' : page }
    return render_to_response('clientes/change_clientes.html', vars,
                                context_instance = RequestContext(request))


@login_required
def detail_clientes(request, id):
    cliente = get_object_or_404(Clientes, pk = id)
    vars = { 'cliente' : cliente }
    return render_to_response('clientes/detail_clientes.html', vars,
                                context_instance = RequestContext(request))


@login_required
def api_get_client(request, id):
    client = get_list_or_404(Clientes, pk=id)
    json = serializers.serialize("json", client)
    return HttpResponse(simplejson.dumps(json), mimetype='application/javascript')


