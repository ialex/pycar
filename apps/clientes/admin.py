from django.contrib import admin
from apps.clientes.models import Clientes


class ClientesAdmin(admin.ModelAdmin):
    pass


admin.site.register(Clientes, ClientesAdmin)
