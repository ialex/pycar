# encoding: utf-8
from django.db import models

class Clientes(models.Model):
    nombre = models.CharField(max_length = 255)
    apellidos = models.CharField(max_length = 255)
    edad = models.IntegerField()
    domicilio = models.CharField(max_length = 255)
    telefono = models.CharField(default=0, max_length=15)
    email = models.EmailField(null=True, blank=True)
    licencia = models.CharField(verbose_name="Numero Licencia", max_length=50)
    lic_vencimiento = models.DateField(verbose_name="Vencimiento de Licencia")
    lic_expedicion = models.DateField(verbose_name="Fecha de Expedición")
    tieneFiscal = models.BooleanField(verbose_name="Requiere Factura?")
    fis_nombre = models.CharField(max_length = 255, blank = True,
        verbose_name="Nombre Fiscal")
    fis_domicilio = models.CharField(max_length = 255, blank=True,
        verbose_name="Domicilio Fiscal", null=True)
    fis_rfc = models.CharField(max_length = 255, blank = True,
        verbose_name="RFC")
    fis_telefono = models.CharField(max_length = 255, blank = True,
        verbose_name="Telefono Fiscal")

    def __unicode__(self):
        return self.nombre + ' ' + self.apellidos

