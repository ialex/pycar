from django.shortcuts import render_to_response, get_object_or_404, redirect, \
    get_list_or_404
from django.contrib.auth.decorators import permission_required, login_required
from django.template import RequestContext
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from apps.autos.models import Autos, Reparaciones
from apps.autos.forms import AutosForm, ReparacionForm
from django.contrib import messages
from apps.eonutils.forms import DivErrorList
from django.core import serializers
from django.http import HttpResponse
from django.utils import simplejson
from django.db.models import Sum
from django.views.generic import ListView


@login_required
def autos(request):
    autos = Autos.objects.filter(active = True).order_by('marca')
    paginator = Paginator(autos, 30) 
    
     # Make sure page request is an int. If not, deliver first page.
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    # If page request (9999) is out of range, deliver last page of results.
    try:
        autos_pagina = paginator.page(page)
    except (EmptyPage, InvalidPage):
        autos_pagina = paginator.page(paginator.num_pages)
    vars = { 'autos' : autos_pagina }
    return render_to_response('autos/autos.html', vars,
                                 RequestContext(request))


@permission_required('autos.delete_autos')
def del_autos(request, id):
    auto = get_object_or_404(Autos, pk = id)
    auto.delete()
    messages.error(request, 'El auto ha sido eliminado.')
    return redirect('/autos/list/')


@permission_required('autos.add_autos')
def add_autos(request):
    if request.method == 'POST':
        form = AutosForm(request.POST, error_class=DivErrorList)
        if form.is_valid():
            form.save()
            messages.success(request, 'El auto ha sido agregado')
            return redirect('/autos/list/', 
                            context_instance = RequestContext(request))
    else:
        form = AutosForm(error_class=DivErrorList)
    vars = {'form' : form}
    return render_to_response('autos/add_autos.html', vars,
                                    context_instance = RequestContext(request))


@permission_required('autos.change_autos')
def edit_autos(request, id):
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
    
    if request.method == 'POST':
        auto = get_object_or_404(Autos, pk = id)
        form = AutosForm(request.POST, instance=auto, error_class=DivErrorList)
        form.save()
        messages.success(request, 'El auto ha sido actualizado.')
        return redirect('/autos/list/?page=' + str(page), 
                            context_instance = RequestContext(request))
    else:
        auto = get_object_or_404(Autos, pk = id)
        form = AutosForm(instance=auto, error_class=DivErrorList)
    vars = { 'form' : form, 'auto' : auto, 'page' : page }
    return render_to_response('autos/change_autos.html', vars,
                                context_instance = RequestContext(request))


@login_required
def api_get_car(request, id):
    car = get_list_or_404(Autos, pk=id)
    json = serializers.serialize("json", car)
    return HttpResponse(simplejson.dumps(json), mimetype='application/javascript')


@permission_required('autos.add_reparaciones')
def add_reparacion(request, id):
    auto = get_object_or_404(Autos, pk=id)
    if request.method == 'POST':
        form = ReparacionForm(request.POST, error_class=DivErrorList)
        if form.is_valid():
            form.save()
            return redirect('/autos/reparaciones/')
    else:
        form = ReparacionForm(initial={'auto': auto}, error_class=DivErrorList)
    vars = {'form': form, 'auto': auto}
    return render_to_response('autos/add_reparacion.html', vars,
                                context_instance = RequestContext(request))


@permission_required('autos.delete_reparaciones')
def del_reparacion(request, id):
    reparacion = get_object_or_404(Reparaciones, pk=id)
    reparacion.delete()
    messages.error(request, 'La reparacion ha sido eliminada.')
    return redirect('/autos/reparacion/detalle/' + id)


@login_required
def detalle_reparacion(request, autoid):
    auto = get_object_or_404(Autos, pk=autoid)
    reparaciones = Reparaciones.objects.filter(auto=auto)
    total = Reparaciones.objects.filter(auto=auto).aggregate(Sum('monto'))
    vars = {'reparaciones': reparaciones, 'auto': auto, 'total': total}
    return render_to_response('autos/detalle_reparacion.html', vars,
                                context_instance = RequestContext(request))


lista_autos = ListView.as_view(model=Autos)
