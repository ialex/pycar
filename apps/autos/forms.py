from django import forms
from apps.autos.models import Autos, Reparaciones

class AutosForm(forms.ModelForm):
    class Meta:
        model = Autos
        exclude = ('active')
        widgets = {
            'color': forms.TextInput(attrs={'class': 'span2'}),
            'anio': forms.TextInput(attrs={'class': 'span2'}),
            'precio': forms.TextInput(attrs={'class': 'span2'})
        }


class ReparacionForm(forms.ModelForm):
    class Meta:
        model = Reparaciones
        widgets = {
            'auto': forms.HiddenInput(),
            'descripcion': forms.Textarea(attrs={'rows': '10', 'cols': '20',
            'class': 'span4'}),
            'proveedor': forms.TextInput(attrs={'class':'span4'}),
            'fecha': forms.TextInput(attrs={'class': 'dp'}),
        }
