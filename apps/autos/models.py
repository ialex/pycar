# encoding: utf-8

from django.db import models
from django.contrib.auth.models import User
from datetime import datetime


class Autos(models.Model):
    Marcas = (
        ('Audi', 'Audi'),
        ('Jeep', 'Jeep'),
        ('Wrangler', 'Wrangler'),
        ('Chevrolet', 'Chevrolet'),
        ('Mercedez', 'Mercedez Benz'),
        ('Ford', 'Ford'),
        ('Nissan', 'Nissan'),
        ('VW', 'VW'),
        ('Fiat', 'Fiat'),
        ('Honda', 'Honda'),
        ('Dodge', 'Dodge'),
        ('Renault', 'Renault'),
        ('Toyota', 'Toyota'),
    )
    codigo = models.CharField(max_length = 55)
    marca = models.CharField(max_length = 255, choices = Marcas)
    modelo = models.CharField(max_length = 255)
    color = models.CharField(max_length = 255)
    anio = models.IntegerField(max_length = 255, verbose_name="Año")
    placas = models.CharField(max_length = 10)
    precio = models.IntegerField(max_length = 255)
    active = models.BooleanField(default = True)

    def __unicode__(self):
        return self.codigo + ' ' + self.placas


class Reparaciones(models.Model):
    auto = models.ForeignKey(Autos)
    descripcion = models.CharField(max_length = 500)
    monto = models.FloatField()
    proveedor = models.CharField(max_length = 255)
    kilometraje = models.IntegerField()
    fecha = models.DateField()

    def __str__(self):
        return self.auto.codigo + ' ' + self.descripcion + ' ' + self.monto
