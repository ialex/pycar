from django.contrib import admin
from apps.autos.models import Autos, Reparaciones

class AutosAdmin(admin.ModelAdmin):
    pass


class ReparacionesAdmin(admin.ModelAdmin):
    pass

admin.site.register(Autos, AutosAdmin)
admin.site.register(Reparaciones, ReparacionesAdmin)
