from django.conf.urls.defaults import patterns, include, url
from apps.autos.views import lista_autos

urlpatterns = patterns('apps.autos.views',
    url(r'list', 'autos'),
    url(r'del/(?P<id>\d+)', 'del_autos'),
    url(r'edit/(?P<id>\d+)', 'edit_autos'),
    url(r'add/$', 'add_autos'),
    url(r'api/get/(?P<id>\d+)$', 'api_get_car'),
    # Reparaciones
    url(r'reparaciones', lista_autos),
    url(r'reparacion/add/(?P<id>\d+)/$', 'add_reparacion'),
    url(r'reparacion/detalle/(?P<autoid>\d+)/$', 'detalle_reparacion'),
    url(r'reparacion/eliminar/(?P<id>\d+)/$', 'del_reparacion'),
)
