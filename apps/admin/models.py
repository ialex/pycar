from django.db import models


class Precios(models.Model):
    hora_extra = models.PositiveIntegerField(default=0,
        verbose_name="Hora Extra")
    seguro_accidentes = models.PositiveIntegerField(default=0, 
        verbose_name="Seguro se accidentes")
    seguro_colision = models.PositiveIntegerField(default=0,
        verbose_name="Seguro se accidentes")
    octavo_gas = models.PositiveIntegerField(default=0,
        verbose_name="Octavo de Gas")
    conductor = models.PositiveIntegerField(default=0, 
        verbose_name="Conductor Adicional")
