from django import forms
from apps.admin.models import Precios


class PreciosForm(forms.ModelForm):
    class Meta:
        model = Precios
