from django.conf.urls.defaults import patterns, include, url


urlpatterns = patterns('apps.admin.views',
    url(r'^config/', 'config'),
    url(r'^api/prices/', 'api_get_prices'),
)
