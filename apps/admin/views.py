# encoding: utf-8
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required, user_passes_test
from django.shortcuts import get_list_or_404
from django.template import RequestContext
from apps.admin.models import Precios
from apps.admin.forms import PreciosForm
from django.contrib import messages
from django.http import HttpResponse
from django.utils import simplejson
from django.core import serializers


@user_passes_test(lambda u: u.is_staff)
def config(request):
    if request.method == 'POST':
        precios = Precios.objects.latest('id')
        form = PreciosForm(request.POST, instance=precios)
        if form.is_valid():
            form.save()
            messages.success(request, 'Configuracion actualizada correctamente')
    else:
        precios = Precios.objects.latest('id')
        if precios:
            form = PreciosForm(instance=precios)
    vars = {'form': form}
    return render_to_response('admin/config.html', vars,
                                    context_instance = RequestContext(request))
        
    
@login_required
def api_get_prices(request):
    prices = get_list_or_404(Precios, id__gt=0)
    json = serializers.serialize("json", prices[-1:])
    return HttpResponse(simplejson.dumps(json), mimetype='application/javascript')

