$(function(){
    
    $('a[href=#]').click(function(){
        return false
    });
    
    // Delete Confirmation
    $('.btn-danger').click(function(){
      var answer = confirm('Realmente quiere realizar esta acción?');
      return answer // answer is a boolean
    });
    
    $('.hotel').popover().click(function(e) {
        e.preventDefault();
      }); 
    
    $('.completeselect').selectToAutocomplete();
    
    $('.dtp').datetimepicker({ 
        dateFormat: 'dd/mm/yy',
        addSliderAccess: true,
        sliderAccessArgs: { touchonly: false },
    });
    
    $('.dp').datepicker({ dateFormat: 'dd/mm/yy' });
    
});
