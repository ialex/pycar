$(function(){
    function toggleFiscal(){
        if ($("#id_tieneFiscal").is(":checked")){
            $('#factura').show('slow');
        }else{
             $('#factura').hide("slow");
        }
    }
    
    function toggleConductor(){
        if ($("#id_tiene_conductor").is(":checked")){
            $('#conductor').show('slow');
        }else{
             $('#conductor').hide("slow");
        }
    }
    
    
    function pago(){
        if ($("#id_tipo_pago").val() == "TARJETA"){
            $('#tarjeta').show('slow');
        }
        if ($("#id_tipo_pago").val() != "TARJETA"){
            $('#tarjeta').hide('slow');
        }
    }
    
    $('#id_tieneFiscal').click(function(){
        toggleFiscal();
    });
    
    $('#id_tipo_pago').change(function(){
        pago();
    });
    
    $('#id_tiene_conductor').click(function(){
        toggleConductor();
    });
    
    toggleFiscal();
    toggleConductor();
    pago();
    
    /*JS Sales */
    function parseDate(str) {
        var mdy = str.split(' ')[0].split('/');
        return new Date(mdy[2], mdy[1]-1, mdy[0]);
    }

    function daydiff(first, second) {
        return (second-first)/(1000*60*60*24)
    }
    
    // Calculate days between two dates
    $("#id_fecha_entrada").change(function(){
        num_days = daydiff(parseDate($('#id_fecha_salida').val()), parseDate($('#id_fecha_entrada').val()));
        $("#id_dias").val(num_days.toPrecision(1));
        $("#dias").text(num_days.toPrecision(1));
    });
    
    // Get price of car
    $("#id_auto").change(function(){
        auto_id = $("#id_auto").val();
        $.getJSON('/autos/api/get/' + auto_id.toString(), function(data){
             json = JSON.parse(data);
             $('#id_preciodia').val(json[0].fields.precio);
        });
    });
    
    
    function showClientInfo(){
        cliente = $("#id_cliente").val();
        if (cliente > 0){
            $.getJSON("/clientes/api/get/" + cliente.toString(), function(data){
                json = JSON.parse(data);
                $("#clienteinfo").fadeOut("slow");
                $("#clienteinfo").empty();
                $("#clienteinfo").append("Num. Licencia: " + json[0].fields.licencia + "<br />");
                $("#clienteinfo").append("Fecha Vencimiento: " +  json[0].fields.lic_vencimiento + "<br />");
                $("#clienteinfo").append("Fecha Expedicion: " + json[0].fields.lic_expedicion + "<br /> <hr />");
                $("#clienteinfo").fadeIn("slow");
            });
        }
    }
    
    
    $('#id_cliente').change(function(){
        showClientInfo();
    });
    
    
    function calculatePrice(){
        total = 0;
        // Convert Response into JSON
        $.getJSON("/panel/api/prices/", function (data){
            json = JSON.parse(data);
            precios = json[0].fields;
                
            pricepday = $("#id_preciodia").val();
            days = $("#id_dias").val();
            
            if(pricepday > 0 && days > 0){
                total = pricepday * days;
            }

            if ($("#id_tiene_conductor").is(":checked")){
                total +=  precios.conductor;
            }

            if ($("#id_seguro_accidentes").is(":checked")){
                total += precios.seguro_accidentes;
            }

            if ($("#id_seguro_colision").is(":checked")){
                total += precios.seguro_colision * days;
            }

            /*
            Calcula el precio de los octavos de gas que se cobraran 
            if($(":radio").is(":checked")){
                total += $('form input[type=radio]:checked').val() * precios.octavo_gas;
            }*/

            if ($("#id_iva").is(":checked")){
                total *= 1.11;
            }
                
            $("#total").text(total.toFixed(2));
            $("#id_total").val(total.toFixed(2));
        });
    }
    
    /* Calculate the total in case there it is an update */
    calculatePrice();
    
    $("#id_auto, #id_dias, #id_fecha_entrada, #id_fecha_salida, #id_adelanto, #id_preciodia").change(function(){
        calculatePrice();
    });
    
    
    $('#id_tiene_conductor, :radio, #id_seguro_accidentes, #id_seguro_colision, #id_iva').click(function(){
        calculatePrice();
    });
});
