from django.conf.urls.defaults import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView


admin.autodiscover()


urlpatterns = patterns('',
    url(r'^autos/', include('apps.autos.urls')),
    url(r'^clientes/', include('apps.clientes.urls')),
    url(r'^ventas/', include('apps.ventas.urls')),
    url(r'^panel/', include('apps.admin.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', TemplateView.as_view(template_name="index.html")),
    url(r'^panel/$', TemplateView.as_view(template_name="index.html")),
)


urlpatterns += patterns('',
    url(r'^panel/login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^panel/logout/$', 'django.contrib.auth.views.logout_then_login'),
)
